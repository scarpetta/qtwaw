/* Copyright (C) 2019-2023 Marco Scarpetta
 *
 * This file is part of QtWAW.
 *
 * QtWAW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtWAW is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QtWAW. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window
import QtWebEngine
import QtCore
import Qt.labs.platform
import QtWAW
import QtQml

ApplicationWindow
{
    id: main_window

    minimumHeight: 400
    minimumWidth: 400
    width: 640
    height: 480
    visible: true

    property int firefox_version: 119
    property string tray_icon: ""
    property string unread_chats: ""
    onUnread_chatsChanged: tray_icon_loader.draw_image()

    Settings {
        property alias x: main_window.x
        property alias y: main_window.y
        property alias width: main_window.width
        property alias height: main_window.height
        property alias tray_icon: main_window.tray_icon
        property alias zoom_factor: web_engine_view.zoomFactor
        property alias select_image_dialog_folder: select_image_dialog.folder
    }

    header: RowLayout
    {
        Button
        {
            id: refresh_button
            icon.name: "view-refresh"
            onClicked: web_engine_view.reloadAndBypassCache()
            ToolTip {text: qsTr("Refresh")}
            Shortcut
            {
                sequences: [StandardKey.Refresh]
                onActivated: refresh_button.clicked()
            }
        }
        Button
        {
            id: zoom_out_button
            icon.name: "zoom-out"
            onClicked: web_engine_view.zoomFactor -= 0.1
            ToolTip {text: qsTr("Zoom out")}
            Shortcut
            {
                sequences: [StandardKey.ZoomOut]
                onActivated: zoom_out_button.clicked()
            }
        }
        Button
        {
            id: zoom_factor
            function update_zoom_factor()
            {
                text = "%1%".arg(Math.round(web_engine_view.zoomFactor * 100))
            }
            Component.onCompleted: update_zoom_factor()
            onClicked: web_engine_view.zoomFactor = 1
            ToolTip {text: qsTr("Reset zoom")}
            Shortcut
            {
                sequences: ["Ctrl+0"]
                onActivated: zoom_factor.clicked()
            }
        }
        Button
        {
            id: zoom_in_button
            icon.name: "zoom-in"
            onClicked: web_engine_view.zoomFactor += 0.1
            ToolTip {text: qsTr("Zoom in")}
            Shortcut
            {
                sequences: [StandardKey.ZoomIn]
                onActivated: zoom_in_button.clicked()
            }
        }
        Image
        {
            id: title_image
            Layout.maximumHeight: title_label.height
            Layout.maximumWidth: title_label.height
        }
        Label
        {
            id: title_label
            Layout.fillWidth: true
        }
        Button
        {
            id: settings_button
            icon.name: "settings-configure"
            onClicked: preferences_dialog.show()
            ToolTip {text: qsTr("Preferences")}
        }
    }

    WebEngineView
    {
        id: web_engine_view
        anchors.fill: parent
        url: "https://web.whatsapp.com"
        profile: WebEngineProfile
        {
            offTheRecord: false
            storageName: "whatsapp"
            persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
            httpUserAgent:
                "Mozilla/5.0 (X11; Linux x86_64; rv:%1.0) Gecko/20100101 Firefox/%1.0".arg(firefox_version)
        }

        onTitleChanged:
        {
            title_label.text = title
            title_image.source = web_engine_view.icon
            var i = title.indexOf('(')
            if (i > -1)
            {
                QtWAW.set_status(true)
                main_window.unread_chats = title.slice(i + 1, title.indexOf(')'))
            }
            else
            {
                QtWAW.set_status(false)
                main_window.unread_chats = ""
            }
        }

        onZoomFactorChanged: zoom_factor.update_zoom_factor()

        property bool first_load: true

        onLoadingChanged: function(info)
        {
            if (first_load && info.status === WebEngineView.LoadSucceededStatus)
            {
                reloadAndBypassCache()
                first_load = false
            }
        }

        Component.onCompleted: {
            grantFeaturePermission("https://web.whatsapp.com",
                                   WebEngineView.Notifications, true);
        }
    }

    Connections
    {
        target: QtWAW

        function onTriggered()
        {
            raise_application()
        }

        function onActivateRequested()
        {
            if (visible)
                hide()
            else
                raise_application()
        }
    }

    onClosing: function (close)
    {
        close.accepted = false
        hide()
    }

    Window
    {
        id: preferences_dialog
        visible: false
        title: qsTr("Preferences")

        GridLayout
        {
            columns: 2
            Label {text: qsTr("Tray icon:")}
            Canvas
            {
                id: tray_icon_loader
                width: 256
                height: 256
                Layout.maximumWidth: 64
                Layout.maximumHeight: 64
                property int font_size: 40
                property int margin: 5
                property string default_icon_path: "qrc:/icon.svg"
                property string last_path: ""
                onLast_pathChanged:
                {
                    if (isImageLoaded(last_path))
                        draw_image()
                    else
                        loadImage(last_path)
                }

                Component.onCompleted:
                {
                    if (main_window.tray_icon.length === 0)
                        last_path = default_icon_path
                    else
                        last_path = main_window.tray_icon
                }
                onImageLoaded: draw_image()

                FileDialog
                {
                    id: select_image_dialog
                    visible: false
                    nameFilters: [qsTr("Image files") + " (*.jpg *.jpeg *.png)"]
                    onAccepted: tray_icon_loader.last_path = file
                }

                MouseArea
                {
                    anchors.fill: parent
                    onClicked: select_image_dialog.open()
                }

                function draw_image()
                {
                    var ctx = getContext("2d")
                    ctx.clearRect(0, 0, width, height)
                    ctx.drawImage(last_path, 0, 0, width, height)

                    if (last_path !== main_window.tray_icon)
                        main_window.tray_icon = toDataURL()

                    if (main_window.unread_chats.length !== 0)
                    {
                        ctx.font = "bold %1px sans-serif".arg(font_size)
                        var tm = ctx.measureText(main_window.unread_chats)
                        var w = tm.width
                        var h = font_size

                        ctx.fillStyle = "#f00"
                        ctx.ellipse(width - (w + 2 * margin), 0,
                                    w + 2 * margin, h + 2 * margin)
                        ctx.fill()

                        ctx.fillStyle = "#fff"
                        ctx.fillText(main_window.unread_chats,
                                     width - (w + margin), h)
                    }

                    QtWAW.set_icon(toDataURL())
                }
            }
            Button
            {
                text: qsTr("Restore default settings")
                onClicked:
                {
                    tray_icon_loader.last_path = tray_icon_loader.default_icon_path
                }
            }
        }
    }

    Shortcut
    {
        sequences: [StandardKey.Quit]
        onActivated: Qt.quit()
    }

    function raise_application()
    {
        show()
        requestActivate()
        raise()
    }
}
