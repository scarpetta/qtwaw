/* Copyright (C) 2019-2023 Marco Scarpetta
 *
 * This file is part of QtWAW.
 *
 * QtWAW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtWAW is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QtWAW. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qtwaw.h"
#include <QMenu>
#include <QIcon>
#include <QApplication>
#include <QDebug>

QtWAW::QtWAW(QObject *parent)
    : QObject{parent}
{
    m_tray_menu = new QMenu{};
    m_status_notifier = new KStatusNotifierItem{};

    QAction *quit_action = new QAction(QIcon::fromTheme("application-exit"),
                                       tr("Quit"),
                                       m_tray_menu);
    quit_action->setShortcut(QKeySequence::Quit);
    connect(quit_action,
            &QAction::triggered,
            qApp,
            &QApplication::quit);

    m_tray_menu->addAction(quit_action);

    m_status_notifier->setContextMenu(m_tray_menu);

    m_status_notifier->setStandardActionsEnabled(false);
    m_status_notifier->setCategory(KStatusNotifierItem::Communications);

    connect(m_status_notifier, &KStatusNotifierItem::activateRequested,
            this, &QtWAW::activateRequested);
}

void QtWAW::set_status(bool active)
{
    if (active)
        m_status_notifier->setStatus(KStatusNotifierItem::Active);
    else
        m_status_notifier->setStatus(KStatusNotifierItem::Passive);
}

void QtWAW::set_icon(const QString &icon_base64)
{
    QString data = icon_base64.right(icon_base64.length() - 22);
    m_status_notifier->setIconByPixmap(
                QPixmap::fromImage(
                    QImage::fromData(
                        QByteArray::fromBase64(
                            data.toStdString().c_str()), "png")));
}

QtWAW::~QtWAW()
{
    delete m_status_notifier;
}
