/* Copyright (C) 2019-2023 Marco Scarpetta
 *
 * This file is part of QtWAW.
 *
 * QtWAW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtWAW is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QtWAW. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QTWAW_H
#define QTWAW_H

#include <QObject>
#include <KF6/KStatusNotifierItem/KStatusNotifierItem>

class QtWAW : public QObject
{
    Q_OBJECT
public:
    explicit QtWAW(QObject *parent = nullptr);

    ~QtWAW();

signals:
    void triggered();

    bool activateRequested();

public slots:
    void set_status(bool active);

    void set_icon(const QString &icon_base64);

private:
    KStatusNotifierItem *m_status_notifier;
    QMenu *m_tray_menu;
};

#endif // QTWAW_H
