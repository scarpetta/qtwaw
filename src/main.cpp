/* Copyright (C) 2019-2023 Marco Scarpetta
 *
 * This file is part of QtWAW.
 *
 * QtWAW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtWAW is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QtWAW. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtWebEngineQuick/QtWebEngineQuick>
#include <QTranslator>
#include <kdbusservice.h>
#include <QDebug>

#include "qtwaw.h"

int main(int argc, char *argv[])
{
    QApplication::setOrganizationName("QtWAW");
    QApplication::setOrganizationDomain("scarpetta.eu");
    QApplication::setApplicationName("qtwaw");
    QApplication::setApplicationVersion("2.0");

    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QtWebEngineQuick::initialize();
    QApplication app(argc, argv);

    app.setApplicationDisplayName("QtWAW");
    app.setDesktopFileName("eu.scarpetta.QtWAW");

    // Set up translations
    QTranslator translator;

    bool ok = translator.load(
                QString("qtwaw_%1.qm").arg(QLocale::system().name()),
                QString("%1/../share/qtwaw/translations").arg(
                    qApp->applicationDirPath()
                    )
                );

    if (ok) app.installTranslator(&translator);

    QtWAW qtwaw{};

    KDBusService service(KDBusService::Unique);
    QObject::connect(&service, &KDBusService::activateRequested,
                     &qtwaw, &QtWAW::triggered);

    qmlRegisterSingletonType<QtWAW>(
                "QtWAW", 1, 0, "QtWAW",
                [&qtwaw](QQmlEngine *, QJSEngine*) -> QObject* {
        QQmlEngine::setObjectOwnership(&qtwaw, QQmlEngine::CppOwnership);
        return &qtwaw;
    });

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
