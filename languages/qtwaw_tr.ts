<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>MainWindow</name>
    <message>
        <source>Quit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>Yakınlaştır</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Uzaklaştır</translation>
    </message>
    <message>
        <source>Original size</source>
        <translation>Orijinal boyut</translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation>CTRL+0</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Yenile</translation>
    </message>
    <message>
        <source>Download completed</source>
        <translation>İndirme tamamlandı</translation>
    </message>
    <message>
        <source>File %1 have beens successfully downloaded</source>
        <translation>%1 dosyası başarıyla indirildi</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Küçültülmüş olarak başlat</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Aç</translation>
    </message>
    <message>
        <source>Close to tray</source>
        <translation>Sistem tepsisine kapat</translation>
    </message>
</context>
</TS>
